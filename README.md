# My Hompage

[![pipeline status](https://gitlab.com/advance-programming-group-3/advprog-project/badges/master/pipeline.svg)](https://gitlab.com/advance-programming-group-3/advprog-project/commits/master)
[![coverage report](https://gitlab.com/advance-programming-group-3/advprog-project/badges/master/coverage.svg)](https://gitlab.com/advance-programming-group-3/advprog-project/commits/master)


## Developer
- Amal Adiguna - 1706018901
- Kenia Visakha - 1706020396
- Muhamad Ilman Nafian - 1706067626
- Muhammad Rizal Alfaridzi - 1706019740

## Links
- [Home](https://gitlab.com/ilmannafian04/advprog-project/wikis/home)
- [CRC Card](https://gitlab.com/ilmannafian04/advprog-project/wikis/Class-Responsibility-Collaboration)
- [UML Diagram](https://gitlab.com/ilmannafian04/advprog-project/wikis/UML-Diagram)
- [User Story](https://gitlab.com/ilmannafian04/advprog-project/wikis/User-Story)
- [PBI](https://docs.google.com/spreadsheets/d/1ak8F62ZosXrX2hF4JpNdYbSZwyH1YPy_wM2bDAx6uvY/edit?usp=sharing)